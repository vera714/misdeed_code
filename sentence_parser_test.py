import stanfordnlp
nlp = stanfordnlp.Pipeline() # Sets up a default neural pipeline in English
doc = nlp("Barack Obama was born in Hawaii. He was elected president in 2008. He walked all the way home, and he shut the door.")

processed_doc = []


def get_string_sentence(s):
    words = []
    for w in s.words:
        words.append(w.text)

    sentence = ' '.join(words)
    sentence = sentence.replace(' .', '.')
    sentence = sentence.replace(' ,', ',')

    return sentence


for s in doc.sentences:
    sentence = get_string_sentence(s)
    processed_doc.append(sentence)


print(processed_doc)
