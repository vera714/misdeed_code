import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import stanfordnlp
import pandas as pd
import re
import tf_sentencepiece
import os
import csv
import time

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'  # run on cpu


def clean_document(d):
    d = re.sub('<.*?>', '', d)
    # d = d.strip()
    return d


def get_string_sentence(s):
    words = []
    for w in s.words:
        words.append(w.text)

    sentence = ' '.join(words)
    sentence = sentence.replace(' .', '.')
    sentence = sentence.replace(' ,', ',')
    return sentence


def get_articles_claims(a_pickle_path, c_pickle_path="", a_txt_path="", c_txt_path=""):
    articles_data = pd.read_pickle(a_pickle_path)

    if a_txt_path != "":
        with open(a_txt_path) as f:
            a_urls = f.readlines()
        a_urls = [x.strip() for x in a_urls]

        articles_data = articles_data[articles_data.url.str.contains('|'.join(a_urls))]

    articles_data = articles_data[['body', 'title', 'url']]
    articles_data = articles_data.dropna(subset=['body'])
    # articles_data = articles_data.iloc[:20]
    articles_data['body'] = articles_data['body'].apply(clean_document)

    print('articles: ', articles_data.shape)

    if c_pickle_path != "":
        claims_all_data = pd.read_pickle(c_pickle_path)
        claims_data = claims_all_data['statement']

        print('claims: ', claims_data.shape)
    elif c_txt_path != "":
        with open(c_txt_path) as f:
            claims_data = f.readlines()
        claims_data = [x.strip() for x in claims_data]
        claims_data = list(set(claims_data))

        print('claims: ', len(claims_data))
    else:
        print("claims path missing")

    claims = []

    for c in claims_data:
        l = []
        l.append(c)
        claims.append(l)

    print("data read")
    return articles_data, claims, claims_all_data

# pustenie nad celym datasetom
articles_data, claims, claims_all_data = get_articles_claims('datasets/monant_articles.p', c_pickle_path='datasets/fact_checks.p')
# pustenie nad datami podla vybranych claimov a url
# articles_data, claims = get_articles_claims('datasets/monant_articles.p', a_txt_path='datasets/all_matched_urls.txt', c_txt_path='datasets/all_matched_claims.txt')
# articles_data, claims = get_articles_claims('datasets/monant_articles.p', c_pickle_path='datasets/fact_checks.p', a_txt_path='datasets/all_matched_urls.txt')



# Graph set up.
g = tf.Graph()
with g.as_default():
    text_input = tf.placeholder(dtype=tf.string, shape=[None])
    print("download going to happen...")
    # ocividne data nestacia teraz na potiahnutie modelu teraz
    embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-multilingual/1")
    print("downloaded")
    embedded_text = embed(text_input)
    init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
g.finalize()

# Initialize session.
session = tf.Session(graph=g)
session.run(init_op)

print(claims_all_data.shape)
claims_all_data['embedded'] = 0
claims_all_data['embedded'] = claims_all_data['statement'].apply(lambda x: session.run(embedded_text, feed_dict={text_input: [x]}))

print('claims embedded')

nlp = stanfordnlp.Pipeline()

match_counter=1

article_match = False
claim_match = False

existing_data = pd.read_csv('outputs/output_new.csv')
last_article = existing_data.iloc[-1]['article url']
last_claim = existing_data.iloc[-1]['claim']

existing_data[:-1].to_csv('outputs/output_new.csv', index=False)
existing_data = None

with open(r'outputs/output_new.csv', 'a', newline='') as csvfile:
    fieldnames = ['article title', 'claim', 'claim rating', 'article url', 'max', 'max sentence', 'mean max5 sentenes', 'max 5 sentences', 'mean', 'a sentences count', 'c words count']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    for article_index, article_row in articles_data.iterrows():

        if article_row['url'] == last_article:
            article_match = True

        if not article_match:
            continue

        article_nlp = nlp(article_row['body'])
        processed_article = []

        for s in article_nlp.sentences:
            sentence = get_string_sentence(s)
            processed_article.append(sentence)

        a_len = len(processed_article)

        enc_article = session.run(embedded_text, feed_dict={text_input: processed_article})

        for claim_index, claim_row in claims_all_data.iterrows():

            print('# ', match_counter)

            if claim_row['statement'] == last_claim:
                claim_match = True
                print("continuation point found")

            if not claim_match:
                continue

            # c = []
            # c.append(claim_row['statement'])
            similarity_matrix = np.inner(enc_article, claim_row['embedded'])
            # print(c)

            similarity_matrix = similarity_matrix.flatten()
            top_indices = similarity_matrix.argsort()[-5:][::-1]
            max_match = similarity_matrix[top_indices[0]]

            mean_similarity = np.mean(similarity_matrix)
            mean_similarity5 = np.mean(similarity_matrix[top_indices])

            max_5_sentences = ""
            for i in top_indices:
                max_5_sentences += ' ' + processed_article[i]
            max_5_sentences = max_5_sentences[:-1]

            # print("max_5_sentences: ", max_5_sentences)

            if max_match > 0.6 or mean_similarity5 > 0.4:
                print()
                # print('Claim')
                # print('- ', c[0])

                print('5 most similar sentences')
                # print(top_indices)



                print()

                print('mean similarity: ', mean_similarity)
                print('mean similarity top 5 sentences: ', mean_similarity5)

            max_5_sentences = ""

            for i in top_indices:
                max_5_sentences += '\n' + processed_article[i]

            max_5_sentences = max_5_sentences[1:]

            # print("max_5_sentences: ", max_5_sentences)

            writer.writerow({
                'article title': article_row['title'],
                 'claim': claim_row['statement'],
                'claim rating': claim_row['rating'],
                'article url': article_row['url'],
                'max': max_match,
                'max sentence': processed_article[top_indices[0]],
                'mean max5 sentenes': mean_similarity5,
                'max 5 sentences': max_5_sentences,
                'mean': mean_similarity,
                'a sentences count': a_len,
                'c words count': len(claim_row['statement'].split()) #ok?
            })

            match_counter += 1


# lenze ja toto neviem potom dalej pustit kde to skoncilo
# vyriesit toto