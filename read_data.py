import pandas as pd
import re
df_claims = pd.read_pickle('datasets/fact_checks.p')
df_claims = df_claims['statement']
df_articles = pd.read_pickle('datasets/monant_articles.p')
df_articles = df_articles['body'].iloc[:1000]
df_articles = df_articles.dropna()


def clean_document(d):
    d = re.sub('<.*?>', '', d)
    return d


print(df_articles.shape)
print(df_claims.shape)

df_articles.apply(clean_document)

document = clean_document(df_articles.iloc[0])
print(type(document))
print(document)


for c in df_claims:
    print(c)