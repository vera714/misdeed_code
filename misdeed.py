import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import stanfordnlp
import pandas as pd
import re
import tf_sentencepiece
import os
import csv
import time

"""


"""

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'  # run on cpu


def clean_document(d):
    d = re.sub('<.*?>', '', d)
    # d = d.strip()
    return d


def get_string_sentence(s):
    words = []
    for w in s.words:
        words.append(w.text)

    sentence = ' '.join(words)
    sentence = sentence.replace(' .', '.')
    sentence = sentence.replace(' ,', ',')
    return sentence


def get_articles_claims(a_pickle_path, c_pickle_path="", a_txt_path="", c_txt_path=""):
    articles_data = pd.read_pickle(a_pickle_path)

    if a_txt_path != "":
        with open(a_txt_path) as f:
            a_urls = f.readlines()
        a_urls = [x.strip() for x in a_urls]

        articles_data = articles_data[articles_data.url.str.contains('|'.join(a_urls))]

    articles_data = articles_data[['body', 'title', 'url']]
    articles_data = articles_data.dropna(subset=['body'])
    # articles_data = articles_data.iloc[:20]
    articles_data['body'] = articles_data['body'].apply(clean_document)

    print('articles: ', articles_data.shape)

    if c_pickle_path != "":
        claims_all_data = pd.read_pickle(c_pickle_path)
        claims_data = claims_all_data['statement']

        print('claims: ', claims_data.shape)
    elif c_txt_path != "":
        with open(c_txt_path) as f:
            claims_data = f.readlines()
        claims_data = [x.strip() for x in claims_data]
        claims_data = list(set(claims_data))

        print('claims: ', len(claims_data))
    else:
        print("claims path missing")

    claims = []

    for c in claims_data:
        l = []
        l.append(c)
        claims.append(l)

    print("data read")
    return articles_data, claims, claims_all_data


# este neotestovane
def encoder_sess(module_url):
    g = tf.Graph()
    with g.as_default():
        text_input = tf.placeholder(dtype=tf.string, shape=[None])
        print("download going to happen...")
        embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-multilingual/1")
        print("downloaded")
        embedded_text = embed(text_input)
        init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
    g.finalize()

    # Initialize session.
    session = tf.Session(graph=g)
    session.run(init_op)

    return session, text_input, embedded_text

# este neotestovane
def encoder_large_sess():
    url = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/1"
    print("large model")
    session, text_input, embedded_text = encoder_sess(url)
    return session, text_input, embedded_text

# este neotestovane
def encoder_small_sess():
    url = "https://tfhub.dev/google/universal-sentence-encoder-multilingual/1"
    print("small model")
    session, text_input, embedded_text = encoder_sess(url)
    return session, text_input, embedded_text


def encoder_qa_sess():
    g = tf.Graph()
    with g.as_default():
        module = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-multilingual-qa/1")
        question_input = tf.placeholder(dtype=tf.string, shape=[None])
        response_input = tf.placeholder(dtype=tf.string, shape=[None])
        response_context_input = tf.placeholder(dtype=tf.string, shape=[None])
        question_embeddings = module(
            dict(input=question_input),
            signature="question_encoder", as_dict=True)

        response_embeddings = module(
            dict(input=response_input,
                 context=response_context_input),
            signature="response_encoder", as_dict=True)

        init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
    g.finalize()

    # Initialize session.
    session = tf.Session(graph=g)
    session.run(init_op)

    return session, question_input, response_input, response_context_input, question_embeddings, response_embeddings


def calc_encodings(df, new_column, original_column, session, embedded_text, text_input):
    df[new_column] = 0
    df[new_column] = df[original_column].apply(lambda x: session.run(embedded_text, feed_dict={text_input: [x]}))

    return df

def calc_encodings_qa(df, new_column, original_column, session, question_embeddings, question_input):
    df[new_column] = 0
    df[new_column] = df[original_column].apply(lambda x: session.run(question_embeddings, feed_dict={question_input: [x]})["outputs"])

    return df


def replace_brackets(x):
    x = x.replace("[", "")
    x = x.replace("]", "")

    return x

def nest_array(x):
    a = np.empty([1,512])
    # print("shape {}".format(a.shape))
    a[0] = x
    # print("shape {}".format(a.shape))

    return a