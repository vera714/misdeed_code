import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import tf_sentencepiece

# Some texts of different lengths.
english_sentences = ["dog", "Puppies are nice.", "I enjoy taking long walks along the beach with my dog."]
italian_sentences = ["cane", "I cuccioli sono carini.", "Mi piace fare lunghe passeggiate lungo la spiaggia con il mio cane."]
japanese_sentences = ["犬", "子犬はいいです", "私は犬と一緒にビーチを散歩するのが好きです"]

# Graph set up.
g = tf.Graph()
with g.as_default():
  text_input = tf.placeholder(dtype=tf.string, shape=[None])
  embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-multilingual/1")
  embedded_text = embed(text_input)
  init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
g.finalize()

# Initialize session.
session = tf.Session(graph=g)
session.run(init_op)

# Compute embeddings.
en_result = session.run(embedded_text, feed_dict={text_input: english_sentences})
it_result = session.run(embedded_text, feed_dict={text_input: italian_sentences})
ja_result = session.run(embedded_text, feed_dict={text_input: japanese_sentences})

print(en_result)
print(it_result)
print(ja_result)

# Compute similarity matrix. Higher score indicates greater similarity.
similarity_matrix_it = np.inner(en_result, it_result)
print(similarity_matrix_it)
max = np.amax(similarity_matrix_it)
print('max: ', max)
pos = np.where(similarity_matrix_it == np.amax(similarity_matrix_it))

similarity_matrix_ja = np.inner(en_result, ja_result)
print(similarity_matrix_ja)
print('max: ', np.amax(similarity_matrix_ja))


print(type(en_result))
print(np.shape(en_result))
print(np.ndim(en_result))

print(type(similarity_matrix_it))
print(np.shape(similarity_matrix_it))
print(np.ndim(similarity_matrix_it))

# zvazit taketo zbiehat z jupyteru nech nemusim vzdy cele pustit

