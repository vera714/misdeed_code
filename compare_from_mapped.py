import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import stanfordnlp
import pandas as pd
import re
import tf_sentencepiece
import os
import csv
import time

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'  # run on cpu


def clean_document(d):
    d = re.sub('<.*?>', '', d)
    # d = d.strip()
    return d


def get_string_sentence(s):
    words = []
    for w in s.words:
        words.append(w.text)

    sentence = ' '.join(words)
    sentence = sentence.replace(' .', '.')
    sentence = sentence.replace(' ,', ',')
    return sentence

articles_data = pd.read_pickle('datasets/monant_articles.p')
# mappings_data = pd.read_pickle('datasets/fact_check_article_mappings1608.p')
# mappings_data = mappings_data.iloc[41063:] TODO zmenit na aktualne cislo


nlp = stanfordnlp.Pipeline()

g = tf.Graph()
with g.as_default():
    text_input = tf.placeholder(dtype=tf.string, shape=[None])
    embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-multilingual/1")
    embedded_text = embed(text_input)
    init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
g.finalize()


# Initialize session.
session = tf.Session(graph=g)
session.run(init_op)

with open(r'outputs/output_mappings_append2.csv', 'a', newline='') as csvfile:
    fieldnames = ['article title', 'claim', 'claim rating', 'article url', 'weight-matus', 'max', 'max sentence', 'mean max 5 sentences', 'max 5 sentences', 'mean', 'article sentences count', 'claim words count']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    for index, row in mappings_data.iterrows():
        print(index)
        found = articles_data.loc[articles_data['url'] == row['article_url']]

        if found['body'].iloc[0] is None:
            continue

        article_nlp = nlp(clean_document(found['body'].iloc[0]))
        processed_article = []

        for s in article_nlp.sentences:
            sentence = get_string_sentence(s)
            processed_article.append(sentence)

        a_len = len(processed_article)

        c = []
        c.append(row['claim_statement'])

        enc_article = session.run(embedded_text, feed_dict={text_input: processed_article})
        enc_claim = session.run(embedded_text, feed_dict={text_input: c})

        similarity_matrix = np.inner(enc_article, enc_claim)
        similarity_matrix = similarity_matrix.flatten()
        top_indices = similarity_matrix.argsort()[-5:][::-1]
        max_match = similarity_matrix[top_indices[0]]

        mean_similarity = np.mean(similarity_matrix)
        mean_similarity5 = np.mean(similarity_matrix[top_indices])

        max_5_sentences = ""
        for i in top_indices:
            max_5_sentences += ' ' + processed_article[i]
        max_5_sentences = max_5_sentences[1:]

        max_5_sentences = ""
        for i in top_indices:
            max_5_sentences += '\n' + processed_article[i]

        # max_5_sentences = max_5_sentences[:-1]

        writer.writerow({
            'article title': row['article_title'],
            'claim': row['claim_statement'],
            'claim rating': row['claim_rating'],
            'article url': row['article_url'],
            'weight-matus': row['weight'],
            'max': max_match,
            'max sentence': processed_article[top_indices[0]],
            'mean max 5 sentences': mean_similarity5,
            'max 5 sentences': max_5_sentences,
            'mean': mean_similarity,
            'article sentences count': a_len,
            'claim words count': len(row['claim_statement'].split()),
        })

