"""
precalculate embeddings:
- regular sentence encoder
- sentence encoder - qa model - claims encoded as questions

adds two columns:
- embedded
- embedded_question

with more claims it can be convenient to calculate them only once
it can be done in code but with multiple runs it is better to have those embeddings already pre-calculated in dataset
"""

import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import pandas as pd
import tf_sentencepiece

import misdeed as m

# get data
articles_data, claims, claims_all_data = m.get_articles_claims('datasets/monant_articles.p', c_pickle_path='datasets/fact_checks.p')

# create session 1
session1, text_input, embedded_text = m.encoder_sess()
# calculate encodings
claims_all_data = m.calc_encodings(claims_all_data, 'embedded', 'statement', session1, embedded_text, text_input)

# create session 2
session2, question_input, _, _, question_embeddings, response_embeddings = m.encoder_qa_sess()
# calculate encodings for claims as questions
claims_all_data = m.calc_encodings_qa(claims_all_data, 'embedded_question', 'statement', session2, question_embeddings, question_input)

claims_all_data.to_pickle('datasets/fact_checks+encodings.p')
claims_all_data2 = pd.read_pickle('datasets/fact_checks+encodings.p')

# toto keby islo o ulozenie ako csv nie ako pickle
# claims_all_data2['embedded'] = claims_all_data2['embedded'].apply(m.replace_brackets)
# claims_all_data2['embedded'] = claims_all_data2['embedded'].apply(lambda x: np.fromstring(x, dtype=float, sep=' '))
# claims_all_data2['embedded'] = claims_all_data2['embedded'].apply(m.nest_array)

# kontrola
print(claims_all_data['embedded'][0][0][:10])
print(claims_all_data2['embedded'][0][0][:10])

print(claims_all_data['embedded_question'][0][0][:10])
print(claims_all_data2['embedded_question'][0][0][:10])
