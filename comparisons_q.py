"""
testing encoding of sentences with question mark and without question mark

"""

"""
encoder

veta s otaznikom
veta s odstranenym otaznikom
veta s otaznikom nahradenym bodkou

"""

import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import pandas as pd
import tf_sentencepiece

import misdeed as m

articles_data, claims, claims_all_data = m.get_articles_claims('datasets/monant_articles.p', c_pickle_path='datasets/fact_checks.p')

# vyfiltruj len vety s otaznikmi
# claims_all_data = claims_all_data[claims_all_data['statement'].str.contains("?$", regex=True)]

print(claims_all_data.shape)

claims_all_data = claims_all_data[claims_all_data['statement'].str.contains("\?")]

# vytvor stlpec bez otaznikv
claims_all_data['statement_no_q'] = claims_all_data['statement'].apply(lambda x: x.replace("?", ""))

# vytvor stlpec s bodkami
claims_all_data['statement_dot'] = claims_all_data['statement'].apply(lambda x: x.replace("?", "."))


# create session 1
session, text_input, embedded_text = m.encoder_small_sess()
# calculate encodings
claims_all_data = m.calc_encodings(claims_all_data, 'embedded_normal', 'statement', session, embedded_text, text_input)

# enc stlpec bez otaznikov
claims_all_data = m.calc_encodings(claims_all_data, 'embedded_no_q', 'statement_no_q', session, embedded_text, text_input)

# enc stlpec s bodkami
claims_all_data = m.calc_encodings(claims_all_data, 'embedded_dot', 'statement_dot', session, embedded_text, text_input)



# porovnanie maly versus velky model

# create session 1
session, text_input, embedded_text = m.encoder_large_sess()
claims_all_data = m.calc_encodings(claims_all_data, 'embedded_large', 'statement', session, embedded_text, text_input)


# uz len zanalyzuj tieto - aka je priemerna podobnost, kde sa to lisi, ako to vyzera

# porovnaj
claims_all_data['normal_vs_noquestion'] = np.vectorize(np.inner)(claims_all_data['embedded_normal'], claims_all_data['embedded_no_q'])
claims_all_data['normal_vs_dot'] = np.vectorize(np.inner)(claims_all_data['embedded_normal'], claims_all_data['embedded_dot'])
claims_all_data['dot_vs_noquestion'] = np.vectorize(np.inner)(claims_all_data['embedded_dot'], claims_all_data['embedded_no_q'])
claims_all_data['normal_vs_large'] = np.vectorize(np.inner)(claims_all_data['embedded_normal'], claims_all_data['embedded_large'])

print(claims_all_data['normal_vs_noquestion'][0])
print(claims_all_data['normal_vs_dot'][0])
print(claims_all_data['dot_vs_noquestion'][0])
print(claims_all_data['normal_vs_large'][0])


print(claims_all_data[['normal_vs_noquestion', 'normal_vs_dot', 'dot_vs_noquestion', 'normal_vs_large']].aggregate(['mean', 'min', 'max']) )