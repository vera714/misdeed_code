import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import stanfordnlp
import pandas as pd
import re
import tf_sentencepiece
import os
import csv
import time

import misdeed as m

# Set up graph.
session, question_input, response_input, response_context_input, question_embeddings, response_embeddings = m.encoder_qa_sess()

# Get data
articles_data, claims, claims_all_data = m.get_articles_claims('datasets/monant_articles.p', c_pickle_path='datasets/fact_checks.p')


print(claims_all_data.shape)
claims_all_data['embedded'] = 0
claims_all_data['embedded'] = claims_all_data['statement'].apply(lambda x: session.run(question_embeddings, feed_dict={question_input: [x]})["outputs"])

print(claims_all_data['embedded'].head())

print('claims embedded')


nlp = stanfordnlp.Pipeline()

match_counter = 1


# article_match = False
# claim_match = False
#
# existing_data = pd.read_csv('outputs/output_new.csv')
# last_article = existing_data.iloc[-1]['article url']
# last_claim = existing_data.iloc[-1]['claim']
#
# existing_data[:-1].to_csv('outputs/output_new.csv', index=False)
# existing_data = None

with open(r'outputs/output_qa1pokus.csv', 'a', newline='') as csvfile:
    fieldnames = ['article title', 'claim', 'claim rating', 'article url', 'max', 'max sentence', 'mean max5 sentenes', 'max 5 sentences', 'mean', 'a sentences count', 'c words count']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    for article_index, article_row in articles_data.iterrows():

        # if article_row['url'] == last_article:
        #     article_match = True
        #
        # if not article_match:
        #     continue

        article_nlp = nlp(article_row['body'])
        processed_article = []

        for s in article_nlp.sentences:
            sentence = m.get_string_sentence(s)
            processed_article.append(sentence)

        a_len = len(processed_article)

        enc_article = session.run(response_embeddings, feed_dict={response_input: processed_article, response_context_input: processed_article})["outputs"]

        for claim_index, claim_row in claims_all_data.iterrows():

            print('# ', match_counter)

            # if claim_row['statement'] == last_claim:
            #     claim_match = True
            #     print("continuation point found")
            #
            # if not claim_match:
            #     continue

            # c = []
            # c.append(claim_row['statement'])
            similarity_matrix = np.inner(enc_article, claim_row['embedded'])
            # print(c)

            similarity_matrix = similarity_matrix.flatten()
            top_indices = similarity_matrix.argsort()[-5:][::-1]
            max_match = similarity_matrix[top_indices[0]]

            mean_similarity = np.mean(similarity_matrix)
            mean_similarity5 = np.mean(similarity_matrix[top_indices])

            max_5_sentences = ""
            for i in top_indices:
                max_5_sentences += ' ' + processed_article[i]
            max_5_sentences = max_5_sentences[:-1]

            # print("max_5_sentences: ", max_5_sentences)

            if max_match > 0.6 or mean_similarity5 > 0.4:
                print()
                # print('Claim')
                # print('- ', c[0])

                print('5 most similar sentences')
                # print(top_indices)



                print()

                print('mean similarity: ', mean_similarity)
                print('mean similarity top 5 sentences: ', mean_similarity5)

            max_5_sentences = ""

            for i in top_indices:
                max_5_sentences += '\n' + processed_article[i]

            max_5_sentences = max_5_sentences[1:]

            # print("max_5_sentences: ", max_5_sentences)

            writer.writerow({
                'article title': article_row['title'],
                 'claim': claim_row['statement'],
                'claim rating': claim_row['rating'],
                'article url': article_row['url'],
                'max': max_match,
                'max sentence': processed_article[top_indices[0]],
                'mean max5 sentenes': mean_similarity5,
                'max 5 sentences': max_5_sentences,
                'mean': mean_similarity,
                'a sentences count': a_len,
                'c words count': len(claim_row['statement'].split()) #ok?
            })

            match_counter += 1




questions = ["What is your age?"]
responses = ["I am 20 years old.", "good morning"]
response_contexts = ["I will be 21 next year.", "great day."]

# Compute embeddings.
question_results = session.run(question_embeddings, feed_dict={question_input: questions})
response_results = session.run(response_embeddings, feed_dict={response_input: responses, response_context_input: response_contexts})

print("question_results")
print(question_results)
print(question_results["outputs"].shape)
print("response_results")
print(response_results)


print("inner")

sim_matrix = np.inner(question_results["outputs"], response_results["outputs"])

print(sim_matrix)
print(sim_matrix.shape)

